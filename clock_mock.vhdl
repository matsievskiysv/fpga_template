library ieee;
use ieee.std_logic_1164.all;

entity clock_mock is

  generic (
    CLOCK_FREQ : integer);

  port (
    clk_out : out std_ulogic);

end entity clock_mock;

architecture simulated of clock_mock is
  constant HALF_TICK : time       := (1 sec) / 2 / CLOCK_FREQ;
  signal clk         : std_ulogic := '0';
begin  -- architecture simulated

  clk_out <= clk;

  process is
  begin  -- process
    wait for HALF_TICK;
    clk <= not clk;
  end process;

end architecture simulated;
