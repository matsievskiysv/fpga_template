# VHDL project template

## Installation

Set build and install directories.

```bash
export INSTALL_DIR=/opt
export BUILD_DIR=/tmp/build
mkdir -p $BUILD_DIR
sudo mkdir -p $INSTALL_DIR
```

### `gtkwave`

Install `gtkwave` simulation viewer from the repository.

```bash
sudo apt install gtkwave
```

### `yosys`

Build `yosys` synthesizer from source.

```bash
cd $BUILD_DIR
sudo apt build-dep yosys
git clone https://github.com/YosysHQ/yosys
pushd yosys
make PREFIX=$INSTALL_DIR/yosys config-gcc
make PREFIX=$INSTALL_DIR/yosys -j $(nproc)
sudo make PREFIX=$INSTALL_DIR/yosys install
echo 'export PATH=$PATH':"$INSTALL_DIR/yosys/bin" >> $HOME/.profile
popd
```

### `icestorm`

Build `icestorm` in order to get Lattice FPGA support.

```bash
cd $BUILD_DIR
sudo apt install libftdi-dev
git clone https://github.com/YosysHQ/icestorm.git
pushd icestorm
sudo mkdir -p $INSTALL_DIR/icestorm
sudo chmod 755 $INSTALL_DIR/icestorm
make PREFIX=$INSTALL_DIR/icestorm -j $(nproc)
sudo make PREFIX=$INSTALL_DIR/icestorm install
echo 'export PATH=$PATH':"$INSTALL_DIR/icestorm/bin" >> $HOME/.profile
popd
```

### `apycula`

Build `apycula` in order to get GoWin FPGA support.

```bash
pip3 install --user apycula
```

### `nextpnr`

Build `nextpnr` place and route tool.

```bash
cd $BUILD_DIR
sudo apt build-dep nextpnr-ice40
git clone https://github.com/YosysHQ/nextpnr.git
pushd nextpnr
cmake -S . -B build -DARCH='ice40;gowin' -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR/nextpnr -DICESTORM_INSTALL_PREFIX=$INSTALL_DIR/icestorm -DCMAKE_BUILD_TYPE=Release
cmake --build build -j $(nproc)
sudo cmake --install build
echo 'export PATH=$PATH':"$INSTALL_DIR/nextpnr/bin" >> $HOME/.profile
popd
```

### `ghdl` and `ghdl-ls`

Build `ghdl` VHDL compiler and VHDL LSP server.

```bash
cd $BUILD_DIR
sudo apt build-dep ghdl
git clone https://github.com/ghdl/ghdl.git
pushd ghdl
./configure --prefix=$INSTALL_DIR/ghdl
make -j $(nproc)
sudo make install
pip3 install --user .
echo 'export PATH=$PATH':"$INSTALL_DIR/ghdl/bin" >> $HOME/.profile
popd
```

### `ghdl-yosys-plugin`

Build `ghdl-yosys-plugin` VHDL module for `yosys`. Make sure that `yosys` binaries are in your `PATH`.

```bash
cd $BUILD_DIR
git clone https://github.com/ghdl/ghdl-yosys-plugin.git
pushd ghdl-yosys-plugin
make -j $(nproc)
sudo env "PATH=$PATH" make install
popd
```

### `openFPGALoader`

Build `openFPGALoader` FPGA upload tool.

```bash
cd $BUILD_DIR
sudo apt install libftdi1-dev
git clone https://github.com/trabucayre/openFPGALoader.git
pushd openFPGALoader
cmake -S . -B build -DCMAKE_INSTALL_PREFIX=$INSTALL_DIR/openFPGALoader -DCMAKE_BUILD_TYPE=Release
cmake --build build -j $(nproc)
sudo cmake --install build
echo 'export PATH=$PATH':"$INSTALL_DIRopenFPGALoader/bin" >> $HOME/.profile
popd
```
