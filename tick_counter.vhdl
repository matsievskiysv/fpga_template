library ieee;
use ieee.std_logic_1164.all;

library work;
use work.config.all;

entity tick_counter is
  port (
    clk, rst : in  std_ulogic;
    count    : out tick_counter_t);
end entity tick_counter;

architecture rtl of tick_counter is
begin  -- architecture rtl

  process (clk, rst) is
    variable counter : tick_counter_t := tick_counter_t'low;
  begin  -- process
    if rst = '0' then                   -- asynchronous reset (active low)
      counter := tick_counter_t'low;
    elsif clk'event and clk = '1' then  -- rising clock edge
      if counter = tick_counter_t'high then
        counter := tick_counter_t'low;
      else
        counter := counter + 1;
      end if;
    end if;
    count <= counter;
  end process;


end architecture rtl;
