library ieee;
use ieee.std_logic_1164.all;

library work;
use work.config.all;

entity blinker_tb is

end entity blinker_tb;

architecture rtl of blinker_tb is

  component blinker is

    port (
      pin_clk, pin_btn, pin_rst : in  std_ulogic;
      pin_led                   : out std_ulogic_vector(LED_COUNT-1 downto 0));

  end component blinker;

  signal clk    : std_ulogic                              := '0';
  signal output : std_ulogic_vector(LED_COUNT-1 downto 0) := (others => '0');

begin

  clock1 : component clock_mock
    generic map (
      CLOCK_FREQ => CLOCK_FREQ)
    port map (
      clk_out => clk);

  blinker1 : component blinker
    port map (
      pin_clk => clk,
      pin_btn => '0',
      pin_rst => '1',
      pin_led => output);

end;
