library ieee;
use ieee.std_logic_1164.all;

package config is

  constant CLOCK_FREQ : integer := 27000000;  -- board clock frequency

  constant PROJECT_FREQ : integer := 5;  -- project clock frequency

  component clock_mock is
    generic (
      CLOCK_FREQ : integer);
    port (
      clk_out : out std_ulogic);
  end component clock_mock;

  component clock_divider is
    generic (
      input_freq  : integer;
      output_freq : integer);
    port (
      clk_in  : in  std_ulogic;
      clk_out : out std_ulogic);
  end component clock_divider;

  constant LED_COUNT : integer := 5;

  subtype tick_counter_t is integer range 0 to (2**LED_COUNT)-1;

  component tick_counter is
    port (
      clk, rst : in  std_ulogic;
      count    : out tick_counter_t);
  end component tick_counter;

end package config;
