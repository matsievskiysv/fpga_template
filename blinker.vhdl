library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.config.all;

entity blinker is

  port (
    pin_clk, pin_btn, pin_rst : in  std_ulogic;
    pin_led                   : out std_ulogic_vector(LED_COUNT-1 downto 0));

end entity blinker;

architecture rtl of blinker is

  signal clk   : std_ulogic;
  signal count : tick_counter_t;

begin

  clock_divider1 : component clock_divider
    generic map (
      input_freq  => CLOCK_FREQ,
      output_freq => PROJECT_FREQ)
    port map (
      clk_in  => pin_clk,
      clk_out => clk);

  tick_counter1 : component tick_counter
    port map (
      clk   => clk,
      rst   => pin_rst,
      count => count);

  pin_led <= std_ulogic_vector(to_unsigned(count, pin_led'length));

end;
