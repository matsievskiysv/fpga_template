library ieee;
use ieee.std_logic_1164.all;

library work;
use work.config.all;

entity tick_counter_tb is

end entity tick_counter_tb;

architecture rtl of tick_counter_tb is
  signal clk, rst : std_ulogic;
  signal count    : tick_counter_t := tick_counter_t'low;
begin  -- architecture rtl

  clock1 : component clock_mock
    generic map (
      CLOCK_FREQ => CLOCK_FREQ)
    port map (
      clk_out => clk);

  clock2 : component clock_mock
    generic map (
      CLOCK_FREQ => CLOCK_FREQ)
    port map (
      clk_out => rst);

  tick_counter1 : component tick_counter
    port map (
      clk   => clk,
      rst   => rst,
      count => count);


end architecture rtl;

configuration tick_counter_test of tick_counter_tb is

  for rtl
    for clock1 : clock_mock
      use entity work.clock_mock
        generic map (
          CLOCK_FREQ => 200000);
    end for;

    for clock2 : clock_mock
      use entity work.clock_mock
        generic map (
          CLOCK_FREQ => 9000);
    end for;
  end for;

end configuration tick_counter_test;
